const TICK = 30;
const ENEMYSPAWNRATE = 1500;
const ENEMY_SPEED = 5;
const ENEMY_WIDTH = 150;
const LIMIT = 17;



function getRandomInt(min, max) 
{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}





class Game 
{
    constructor() 
    {
        this.gameArea = document.getElementById("game_area");
        this.player = document.getElementById("player");
        this.score = document.getElementById("score");
        this.text = document.getElementById("text_area");

        this.width = this.gameArea.clientWidth;

        console.log({ w: this.width, p: this.player });
        this._resetPlayer();
    }



    start() 
    {
        this.addEventListeners();
    }



    addEventListeners() 
    {
        var startButton = document.getElementById("start");
        var restartButton = document.getElementById("restart");

        document.removeEventListener("keydown", this.movePlayer);
        document.addEventListener("keydown", this.movePlayer.bind(this));

        startButton.addEventListener("click", () => {
            if (!this.running) this._start();
        });

        restartButton.addEventListener("click", () => {
            this.restart();
        });
    }

    _start() 
    {
        if (this.enemies) {
            this.enemies.forEach((e) => {
                this.gameArea.removeChild(e);
            });
            this.enemies = null;
        }

        if (this.retry) {
            this._resetPlayer();
        }

        this.text.innerText = "";
        this.score.innerText = 0;

        
        this.running = true;
        this.hasFailed = false;

        this.gameArea.style.borderColor = "rgb(243, 210, 103)";

        console.log("game started");

        this.enemies = [this._spawn()];

        let count = 0;
        var lastEnemey;

        this.tick = setInterval(() => {
            this.enemies.forEach((e) => {
                var pos = e.style.top.replace("px", "");
                pos -= ENEMY_SPEED;

                if (pos < 0) 
                {
                    this.gameArea.removeChild(this.enemies.shift());
                } 
                
                else 
                {

                    // enemy height
                    var player_x = this._getPlayerPos();
                    var enemy_x = this._getXPos(e);
                    var enemy_y = pos / 10;

                    console.log({
                        player_x,
                        right: enemy_x,
                        left: enemy_x + ENEMY_WIDTH / 10,
                    });

                    if (enemy_y === 3) 
                    {
                        if (
                            player_x <= enemy_x ||
                            player_x >= enemy_x + ENEMY_WIDTH / 10
                        ) 
                        {
                            this.fail();
                            return;
                        } 
                        else 
                        {
                            if (lastEnemey === e) 
                            {
                                console.log("is last :)");
                                this.win();
                            }
                            this.score.innerText = +this.score.innerText + 1;
                        }
                    }

                    e.style.top = pos + "px";
                }
            });

            // console.log('tick');
        }, TICK);

        this.enemySpawn = setInterval(() => 
        {
            count++;
            if (count > LIMIT) 
            {
                return;
            }
            var isLast = count === LIMIT;
            var en = this._spawn(isLast);
            if (isLast) 
            {
                lastEnemey = en;
            }
            this.enemies.push(en);
        }, ENEMYSPAWNRATE);
    }




    _spawn(finish) 
    {
        var last = this.enemies && this.enemies[this.enemies.length - 1];

        var isStart = false;

        var x; // new left x;

        if (!last) 
        {
            last = this.player;

            // center first enemy
            x = this._getPlayerPos() - (ENEMY_WIDTH / 10 / 3) * 1.5 - 1;
            x = Math.floor(x);
            isStart = true;
        } 
        else 
        {
            x = this._getXPos(last);
        }

        const maxWidth = this.width / 10;
        const enemyWidth = ENEMY_WIDTH / 10;

        console.log(x);

        // create possbile new range
        if (!isStart) 
        {
            var left = Math.random() < 0.5 ? true : false;
            if (left) 
            {
                x = x - enemyWidth / 3;
            } 
            
            else 
            {
                x = x + enemyWidth / 3;
            }
        }


        // check left and right boundrys
        // exceeding left boundry
        if (x < 0) 
        {
            console.log("is min");
            x = 0;
            // exceeding right boundry
        } 
        
        else if (x >= maxWidth - enemyWidth) 
        {
            console.log("is max");
            x = maxWidth - enemyWidth - 2;
        }
        console.log("fixed x to", x);

        var enemy = document.createElement("div");
        enemy.classList.add("enemy", "game-entity");
        if (finish) 
        {
            enemy.classList.add("finish");
        }

        enemy.style.top = this.gameArea.clientHeight + "px";

        enemy.style.left = x * 10 + "px";

        this.gameArea.appendChild(enemy);

        return enemy;
    }

    stop() 
    {
        this.running = false;
        this.retry = true;
        clearInterval(this.tick);
        clearInterval(this.enemySpawn);
    }

    restart() 
    {
        this.stop();
        this._resetPlayer();
    }

    fail() 
    {
        this.stop();
        this.gameArea.style.borderColor = "red";
        this.hasFailed = true;
        this.text.innerText = "Verloren :(";
    }

    win() 
    {
        this.stop();
        this.text.innerText = "Gewonnen :)";
    }

    
    movePlayer(e) 
    {
        var tiles = this.width / 10;
        var pos = this._getPlayerPos();

        if (e.key === "ArrowLeft") {
            pos--;
        } else if (e.key === "ArrowRight") {
            pos++;
        }

        if (pos >= tiles || pos < 0) {
            this.fail();
        } else {
            this._setPlayerPos(pos);
        }
    }

    _resetPlayer() 
    {
        var enWidth = ENEMY_WIDTH / 10;
    
        this._setPlayerPos(getRandomInt(enWidth, this.width / 10 - enWidth));
        this.player.style.display = "block";
    }

    _getYPos(entity) 
    {
        return +entity.style.top.replace("px", "") / 10;
    }

    _getXPos(entity) 
    {
        return +entity.style.left.replace("px", "") / 10;
    }

    _getPlayerPos() 
    {
        return +this.player.style.left.replace("px", "") / 10;
    }
    _setPlayerPos(pos) 
    {
        this.player.style.left = pos * 10 + "px";
    }
}

var game = new Game();
game.start();
